import numpy as np


def pca(X):
    """computes eigenvectors of the covariance matrix of X
      Returns the eigenvectors U, the eigenvalues (on diagonal) in S
    """
    
    m, n = X.shape
    covX = np.matmul(X.T,X)/float(m)
    U,S,V = np.linalg.svd(covX)

    return U, S, V