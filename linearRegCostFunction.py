import numpy as np
def linearRegCostFunction(X, y, theta, Lambda):
    """computes the
    cost of using theta as the parameter for linear regression to fit the
    data points in X and y. Returns the cost in J and the gradient in grad
    """

    m = y.size # number of training examples

    J = 1./2./m*sum((np.matmul(X,theta)-y)**2)+Lambda/m/2*sum(theta[1:]**2)
    grad = 1./m*np.matmul(X.T,(np.matmul(X,theta)-y))
    grad[1:] += Lambda/m*theta[1:]
# =========================================================================

    return J, grad

def linearRegCostFunctionLasso(X, y, theta, Lambda):
    """computes the
    cost of using theta as the parameter for linear regression to fit the
    data points in X and y. Returns the cost in J and the gradient in grad
    """

    m = y.size # number of training examples
    
    J = 1./2./m*sum((np.matmul(X,theta)-y)**2)+Lambda/m/2*sum(abs(theta[1:]))
    grad = 1./m*np.matmul(X.T,(np.matmul(X,theta)-y))
    grad[1:] += Lambda/m*(2*np.heaviside(theta[1:],1)-1)
# =========================================================================

    return J, grad