#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Fri Oct 11 09:50:52 2019

@author: yelena
"""

import matplotlib.pyplot as plt
import seaborn as sns

class MyPlotterClass:
    
    pos = [1920, 28, 1920, 1052]
    size_inches = [14*1.5,10*1.5]
    
    def __init__(self, newpos=[1920, 28, 1920, 1052]):
        self.pos = newpos
        
    def initFig(self):
        plt.figure()
        try:
            plt.get_current_fig_manager().window.setGeometry(self.pos[0],self.pos[1],self.pos[2],self.pos[3]) #Make fullscreen
        except AttributeError:
            plt.gcf().set_size_inches(self.size_inches)
            
    def plotDists(self,df,varlist,ilog=False):
        """Distributions of 3 variables and SalePrice"""
        self.initFig()       
        for i in range(len(varlist)):
            plt.subplot(2,2,i+1)
            df[varlist[i]].hist(log = ilog)
            plt.xlabel(varlist[i])
            plt.ylabel('Counts')
            
    def plotDists2(self,X,varlist):
        m,n=X.shape
        for i in range(n):
            if (i%6==0):
                self.initFig()
            plt.subplot(2,3,(i%6)+1)
            plt.hist(X[:,i])
            plt.title(varlist[i])
            plt.ylabel('Counts')
        
    def plotResiduals(self, X,y,Xval,yval,theta):
        res_train = X@theta-y
        res_val = Xval@theta - yval

        fig = plt.figure(figsize=(8,4))

        plt.subplot(1,2,1)
        _,xbins,_ = plt.hist(res_train,bins=20,histtype='step',density=True,log=True);
        plt.hist(res_val,bins=xbins,histtype='step',density=True);
        plt.ylabel('# of Houses')
        plt.xlabel('$\log(y)-\log(\hat{y})$')
        plt.legend(['Training','Validation'])
        plt.title('Residuals Distribution')

        ax = plt.subplot(1,2,2)
        plt.scatter(y,res_train,s=2)
        plt.scatter(yval,res_val,s=2)
        plt.ylabel('$\log(y)-\log(\hat{y})$');
        plt.xlabel('$\log(y)$');
        #plt.legend(['Training','Validation'])
        plt.title('Residual vs Value')
        #ax.set(ylim=(-1,1))

#         ax = plt.subplot(1,3,3)
#         plt.scatter(y,res_train,s=2)
#         plt.scatter(yval,res_val,s=2)
#         plt.ylabel('$\log(y)-\log(\hat{y})$');
#         plt.xlabel('$\log(y)$');
#         plt.legend(['Training','Validation'])
#         plt.title('Error vs Value (Full Range)');
        
        plt.tight_layout()

        return fig
    
    def plotErrorVsParams(self,X,y,Xval,yval,theta,varlist):
        res_train = X@theta-y
        res_val = Xval@theta - yval
        
        m,n=X.shape
        for i in range(1,n):
            if ((i-1)%6==0):
                self.initFig()
            plt.subplot(2,3,((i-1)%6)+1)
            plt.scatter(X[:,i],res_train,s=2)
            plt.scatter(Xval[:,i],res_val,s=2)
            #ax = sns.stripplot(X[:,i],res_train,jitter=True,dodge = True)
            #ax = sns.violinplot(X[:,i],res_train,color='r')
            #plt.setp(ax.collections, alpha=.0)
            #ax = sns.stripplot(Xval[:,i],res_val,jitter=True,dodge = True)
            #ax = sns.violinplot(X[:,i],res_train,color='b')
            #plt.setp(ax.collections, alpha=.0)
            plt.title(varlist[i])
            plt.ylabel('$\log(y)-\log(\hat{y})$');
            
        
            
    def plotScatter(self,df,varlist):    
        """Scatter plots of Sale Price vs 3 variables"""
        self.initFig()
        for i in range(len(varlist)):
            plt.subplot(2,2,i+1)
            plt.scatter(df[varlist[i]],df['SalePrice'])
            plt.xlabel(varlist[i])
            plt.ylabel('SalePrice')
            
        
    def plotBoxplot(self,df,varlist):
        """Boxplots of Sale Price vs 3 categorical variables"""

        self.initFig()
        for i in range(len(varlist)):
            plt.subplot(2,2,i+1)
            ax= sns.boxplot(x=varlist[i],y='SalePrice',data=df)
            sum_index = df.groupby([varlist[i]])['SalePrice'].median().values
            sum_labels = df.groupby([varlist[i]])[varlist[i]].count().values
            pos1 = range(len(sum_labels))
            for tick,label in zip(pos1,ax.get_xticklabels()):
                ax.text(pos1[tick], sum_index[tick], sum_labels[tick], 
                        horizontalalignment='center', verticalalignment='center', size='x-large', color='w', weight='semibold')


    def plotCorrleations(self,df):
        cov = df.cov() #covariance matrix: C_ij=sum[(xik - mui)(xjk-muj)]/(m-1), where m is the number of entries
        corr = df.corr() #corellation matrix: R_ij=C_ij/sqrt(C_ii C_jj) = C_ij/(sig_i*sig_j)

        self.initFig()
        sns.set(font_scale=.8)
        ax = sns.heatmap(abs(corr),xticklabels=True,yticklabels=True)
        plt.title('Correlation Heatmap',fontdict={'fontsize':34})
        
        #create correlation chart of SalePrice vs variables
        sp_corr = abs(corr['SalePrice'])
        sp_corr.drop('SalePrice',inplace=True)#remove self-correlation term
        sp_corr.dropna(inplace=True)#drop NaNs
        sp_corr.sort_values(ascending=False,inplace=True)

        #Visual of ordered corrlations 
        self.initFig()
        sns.set(font_scale=1.2)
        #plt.get_current_fig_manager().window.setGeometry(pos[0],pos[1],pos[2],pos[3]) #Make fullscreen
        sp_corr.plot.bar()
        plt.title('Ordered Features Correlated with SalePrice',fontdict={'fontsize':34})

        plt.style.use('yb_pres') #somehow sns.set(font_scale) overwrites the style
    
    