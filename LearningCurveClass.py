import numpy as np

from trainLinearReg import trainLinearReg
from linearRegCostFunction import linearRegCostFunction,linearRegCostFunctionLasso
from dataFuncs import get_test_train_sets
from MyPlotterClass import MyPlotterClass
import matplotlib.pyplot as plt
from computeFitStats import computeFitStats

class LearningCurveClass:
    
    err_rel=0 #relative error in price target
    error_target=0 # in log
    glabel='' #labels for targets
    exclude_outs_from_MSE = False
    reg = 'L2'
    
    def __init__(self):
        self.err_rel = np.array([.01, .05, .1]) #relative error in price target
        self.error_target = np.log(1-self.err_rel)**2
        self.glabel = [str(self.err_rel[i]*100)+'% Goal' for i in range(len(self.err_rel))]
    
    def set_targeterror(self,err_rel):
        self.err_rel = err_rel
        self.error_target = np.log(1-err_rel)**2
        self.glabel = [str(err_rel[i]*100)+'% Goal' for i in range(len(err_rel))]
        
    def set_MSE_exclude_Id(self,outlier_Ids):
        self.outlier_Ids = outlier_Ids
        self.exclude_outs_from_MSE = True
        
    def set_reg(self,ireg):
        if ireg=='L1' or ireg=='L2':
            self.reg = ireg
        else:
            print('Unrecognizable regulation setting. Regulation type set to ' + self.reg)
            
    
    def learningCurve(self,X, y, Xval, yval, Lambda):
        """returns the train and
        cross validation set errors for a learning curve. In particular,
        it returns two vectors of the same length - error_train and
        error_val. Then, error_train(i) contains the training error for
        i examples (and similarly for error_val(i)).

        In this function, you will compute the train and test errors for
        dataset sizes from 1 up to m. In practice, when working with larger
        datasets, you might want to do this in larger intervals.
        """
        # Number of training examples
        m, n = X.shape

        # Initialize Theta
        initial_theta = np.random.rand(n)
        initial_theta[0]=50000


        step = max(int(m/50),1)
        n_ex = np.array(range(step,m,step))
        error_train = np.zeros(len(n_ex))
        error_val   = np.zeros(len(n_ex))

        for i,val in enumerate(n_ex):
            result = trainLinearReg(X[0:(val+1)], y[0:(val+1)], Lambda,initial_theta,labels=None,maxiter=2000,plotprogress=False,disp=False,regtype = self.reg)
            theta = result.x

            error_train[i], _ =linearRegCostFunction(X[0:(val+1)],y[0:(val+1)],theta,0)
            error_val[i], _ = linearRegCostFunction(Xval,yval,theta,0)

        return (error_train, error_val, n_ex)
    #     return (error_train, error_val)

    def validationCurve(self,X,y,Xval,yval,Lambda):
        """returns train and cross validation set errors as a function of Lambda"""    # Number of training examples
        m, n = X.shape

        # Initialize Theta
        initial_theta = np.random.rand(n)
        #initial_theta = np.ones(n)
        initial_theta[0]=50000

        error_train = np.zeros(len(Lambda))
        error_val   = np.zeros(len(Lambda))

        for i,val in enumerate(Lambda):
            result = trainLinearReg(X, y,val,initial_theta,labels=None,maxiter=2000,plotprogress=False,disp=False,regtype = self.reg)
            theta = result.x

            error_train[i], _ =linearRegCostFunction(X,y,theta,0)
            error_val[i], _ = linearRegCostFunction(Xval,yval,theta,0)

        return (error_train, error_val, Lambda) 


    def plotErrorCurve(self, n_ex,error_train,error_val,plotGoals = False):
        fig = plt.figure()
        plt.plot(n_ex,error_train,'o-')
        plt.plot(n_ex,error_val,'o-')
        if plotGoals:
             [plt.plot(n_ex,self.error_target[i]*np.ones(len(n_ex))) for i in range(len(self.err_rel))]
        plt.ylabel('MSE')
        plt.legend(['Training','Validation']+self.glabel);

    
    def resampleFit(self,n_trials,df_num,mu,std,icv_frac,Lambda,plotDists=True):
        X,y,Xval,yval,_,_,labels = get_test_train_sets(df_num,mu,std,cv_frac=0.1)#includes bias
        Theta = np.zeros((n_trials,X.shape[1]))
        error_val = np.zeros((n_trials,1))
        initial_theta = np.random.rand(X.shape[1])
        initial_theta[0]=10
        myplots = MyPlotterClass()

        for i in range(n_trials):
            X,y,Xval,yval,_,Id_val,_ = get_test_train_sets(df_num,mu,std,cv_frac=0.1)#includes bias
            result = trainLinearReg(X, y, Lambda,initial_theta,labels,maxiter=2000,plotprogress=False,disp=False,regtype = self.reg)
            theta = result.x
            Theta[i,:] = theta
            if self.exclude_outs_from_MSE:
                ind = np.in1d(Id_val.values, self.outlier_Ids.values)
                error_val[i],_ = linearRegCostFunction(Xval[~ind,:],yval[~ind],theta,0)
#                 if ind.size !=0:
#                     error_val[i],_ = linearRegCostFunction(Xval[~ind,:],yval[~ind,:],theta,0)
#                 else:
#                     error_val[i],_ = linearRegCostFunction(Xval,yval,theta,0)
            else:
                error_val[i],_ = linearRegCostFunction(Xval,yval,theta,0) 
            #chi2[i],dof,SE=computeFitStats(result,Xval,yval)#think about this more

        if plotDists:
            myplots.plotDists2(Theta,labels)
        plt.figure()
        plt.hist(error_val,bins = np.arange(0,.06,.001))
        plt.xlabel('MSE')
        plt.ylabel('Counts')

        #compute mean and std
        theta_mu = Theta.mean(axis=0)
        theta_se = Theta.std(axis=0)
        mse_val = error_val.mean()
        mse_se = error_val.std()


        return theta_mu,theta_se,mse_val,mse_se

    def singleFit(self,X,y,Xval,yval,Lambda,labels):
        
        #Initialize theta
        #initial_theta = np.ones(X.shape[1])
        initial_theta = np.random.rand(X.shape[1])
        initial_theta[0]=10


        result = trainLinearReg(X, y, Lambda,initial_theta,labels,maxiter=2000,plotprogress=True,disp=True, plotlegend=False,regtype = self.reg)
        theta = result.x
        error_train,_ = linearRegCostFunction(X,y,theta,0)
        error_val,_ = linearRegCostFunction(Xval,yval,theta,0)

        print("Train MSE:\t %.4f" % (error_train))
        print("Validation MSE:\t %.4f" % (error_val))
        print("%s:\t %.4f" % (self.glabel[2], self.error_target[2]))
        print("%s:\t %.4f" % (self.glabel[1], self.error_target[1]))

        # chi2,dof,SE = computeFitStats(result,X,y)
        # print_labels = ['Bias'] + labels
        #print("Parameter \t Value \t SE")
        #[print("%s \t %.2e \t %.2e" % (print_labels[i],theta[i],SE[i])) for i in range(theta.shape[0])]
        # import statsmodels.api as sm
        # model = sm.OLS(y,X).fit()
        # [print("%s\t%.3f\t%.3f\t%.3f\t%.3f" % (print_labels[i],theta[i],model.params[i],SE[i],model.bse[i])) for i in range(theta.shape[0])]
        
        return result, error_train, error_val
    
    def computeValidationCurveWithResampling(self,n_trials,Lambda_array,df_num,mu,std):

        plt.figure()
        #plot target MSE values
#         [plt.plot(Lambda_array,self.error_target[i]*np.ones(len(Lambda_array))) for i in range(len(self.err_rel))]
        #compute validation curves for n_trials
        for i in range(n_trials):
            X,y,Xval,yval,_,_,_ = get_test_train_sets(df_num,mu,std,cv_frac=0.1)#includes bias
            _, error_val, _ = self.validationCurve(X, y, Xval, yval, Lambda_array)
            plt.plot(Lambda_array,error_val,'o-')
            
        plt.xlabel('$\lambda$')
        plt.ylabel('MSE')
        plt.title('MSE vs $\lambda$ for ' + str(n_trials) + ' trials.')
        #plt.legend(self.glabel);
    
