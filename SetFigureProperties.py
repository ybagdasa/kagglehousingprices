#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 27 15:08:27 2019

@author: yelena
"""
import matplotlib.pyplot as plt

def SetFigureProperties(gtitlesize=16,glabelsize=16,gticklabelsize=15, 
             legsize=15, glinewidth=2.0, gmarkersize=6.0):
    ax=plt.gca();
    ax.tick_params(labelsize=gticklabelsize)
    ax.xaxis.label.set_size(glabelsize)
    ax.yaxis.label.set_size(glabelsize)
    ax.zaxis.label.set_size(glabelsize)
    ax.title.set_fontsize(gtitlesize)
    try: 
        plt.setp(ax.get_legend().get_texts(), fontsize=legsize) #legend 'list' fontsize
    except:
        pass
        
    plt.setp(ax.lines, linewidth=glinewidth)
    plt.setp(ax.lines, markersize=gmarkersize)
    plt.setp(ax.lines, fillstyle='none')
