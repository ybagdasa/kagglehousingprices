#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Standardizes and normalizes entries of dataframe, such that mean of each feature is zero and the std of each feature is 1.
Created on Thu Oct 10 12:53:18 2019

@author: yelena
"""

def featureNormalize(din, imu=None,istd=None):
    if (imu is None) and (istd is None):
        mu = din.mean(axis=0)
        std = din.std(axis=0)
    else:
        mu = imu
        std = istd
    
    dout = din-mu
    dout = dout/std
    return (dout,mu,std)