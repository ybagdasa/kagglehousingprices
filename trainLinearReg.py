from scipy.optimize import minimize
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np

from linearRegCostFunction import linearRegCostFunction,linearRegCostFunctionLasso

def PlotProgress(xk,lObjects,lCost,costFunction):

    it = lObjects[0].get_xdata();
    new_it = it[-1]+1;
    for i,param in enumerate(xk[1:]):
        lObjects[i].set_xdata(np.append(lObjects[i].get_xdata(), new_it))
        lObjects[i].set_ydata(np.append(lObjects[i].get_ydata(), param))
        
    #Plot cost function
    cost = costFunction(xk)
    lCost.set_xdata(np.append(lCost.get_xdata(), new_it))
    lCost.set_ydata(np.append(lCost.get_ydata(), cost))
    
def trainLinearReg(X, y, Lambda, initial_theta, labels=None, method='CG', maxiter=200, plotprogress=True, disp=True, plotlegend = True, regtype='L2'):

    """trains linear regression using
    the dataset (X, y) and regularization parameter lambda. Returns the
    trained parameters theta.
    """

# Create "short hand" for the cost function to be minimized
    if regtype=='L2':
        costFunction = lambda t: linearRegCostFunction(X, y, t, Lambda)[0]
        gradFunction = lambda t: linearRegCostFunction(X, y, t, Lambda)[1]
    elif regtype=='L1':
        costFunction = lambda t: linearRegCostFunctionLasso(X, y, t, Lambda)[0]
        gradFunction = lambda t: linearRegCostFunctionLasso(X, y, t, Lambda)[1]
    else :
        print('Unrecognizable regularization type. Setting to L2.')
        costFunction = lambda t: linearRegCostFunction(X, y, t, Lambda)[0]
        gradFunction = lambda t: linearRegCostFunction(X, y, t, Lambda)[1]



    if plotprogress:    
        fpar = plt.figure(figsize=[14.17,  4.8 ])
        ax = plt.subplot(1,2,1)
        ax2 = plt.subplot(1,2,2)
        colors = cm.gist_rainbow(np.linspace(0, 1, len(initial_theta)))
        LINE_STYLES = ['solid', 'dashed', 'dashdot', 'dotted']
        NUM_STYLES = len(LINE_STYLES)
        j=0 #index of parameter
        
        #Initialize line object list
        lObjects= [None]*len(initial_theta[1:])
        for t0, c in zip(initial_theta[1:], colors):
            myline, = ax.plot(1, t0,'o-', color=c) #comma after myline is important!
            myline.set_linestyle(LINE_STYLES[j%NUM_STYLES])
            lObjects[j]=myline;
            j+=1;
        #Plot initital cost function value
        lCost, = ax2.plot(1,costFunction(initial_theta))  
        plotFunction = lambda t: PlotProgress(t,lObjects,lCost,costFunction)
    else:
        plotFunction = lambda t: None
    
    
    result = minimize(costFunction, initial_theta, tol = 1e-5, method=method, jac=gradFunction, options={'disp': disp, 'maxiter': maxiter},callback=plotFunction)

    if plotprogress:
        #Finialize Plot
        ax.relim()
        ax.autoscale_view()
        ax.set_position([0.125, 0.11, 0.25, 0.80])
        if plotlegend:
            lg = ax.legend(labels, ncol=2, loc='upper right',bbox_to_anchor=(.5,.9), bbox_transform=plt.gcf().transFigure)
        ax.set_xlabel('Iteration')
        ax.set_ylabel(r'$\theta$')
        ax.set_title(r'$\theta$ vs Iteration')

        ax2.relim()
        ax2.autoscale_view()
        ax2.set_xlabel('Iteration')
        ax2.set_ylabel('Cost Function')
        ax2.set_title('Cost vs Iteration')

        plt.draw()           
    
    #compute statistics
    
    return (result)
