This is a python repo of my current progress on the following Kaggle: https://www.kaggle.com/c/house-prices-advanced-regression-techniques

The goal of this project is to familiarize myself with dataframes and optimization routines in python. This project is a work in progress.

The main script is **NumRegression.ipynb**. 

