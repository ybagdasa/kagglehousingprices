import pandas as pd
from pandas.api.types import CategoricalDtype
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

from featureNormalize import featureNormalize
from linearRegCostFunction import linearRegCostFunction
from linearRegLogCostFunction import linearRegLogCostFunction
from trainLinearReg import trainLinearReg
from plot2DFit import plot2DFit
from MyPlotterClass import MyPlotterClass
from dataFuncs import loadAndConvertData, lookAtMissingData



pos = [1920, 28, 1920, 1052]

myplots = MyPlotterClass(pos)
plt.style.use('yb_pres')
#from scipy.stats import norm
#from sklearn.preprocessing import StandardScaler
#from scipy import stats


#%%Load Data######################################################################
df,df_labels = loadAndConvertData('train.csv','train_labels.csv')
#%% create correlation heat map of variables

cov = df.cov() #covariance matrix: C_ij=sum[(xik - mui)(xjk-muj)]/(m-1), where m is the number of entries
corr = df.corr() #corellation matrix: R_ij=C_ij/sqrt(C_ii C_jj) = C_ij/(sig_i*sig_j)

plt.figure()
plt.get_current_fig_manager().window.setGeometry(pos[0],pos[1],pos[2],pos[3]) #Make fullscreen
sns.set(font_scale=.8)
ax = sns.heatmap(abs(corr),xticklabels=True,yticklabels=True)

#create correlation chart of SalePrice vs variables
sp_corr = abs(corr['SalePrice'])
sp_corr.drop('SalePrice',inplace=True)#remove self-correlation term
sp_corr.dropna(inplace=True)#drop NaNs
sp_corr.sort_values(ascending=False,inplace=True)

#Visual of ordered corrlations 
plt.figure()
plt.get_current_fig_manager().window.setGeometry(pos[0],pos[1],pos[2],pos[3]) #Make fullscreen
sp_corr.plot.bar()

#%% Look at distributions of top 3 variables and Sales Price
plt.style.use('yb_pres') #somehow sns.set(font_scale) overwrites the style
myplots.plotDists(df,list(sp_corr.index)[0:4])
myplots.plotScatter(df,list(sp_corr.index)[0:4])
#%%missing data is nan (or -1 for converted numerical labels)

df_train = df

missing_data = lookAtMissingData(df_train)
#%%


#myplots.plotDists(df,list(miss_num.index)[0:3])
myplots.plotBoxplot(df,list(missing_data.index)[0:4])
plt.suptitle('SalePrice vs Missing Numerical Fields')
#PoolQC:with only 7 homes that have a pool, remove column
#MiscFeature-clearly not needed, remove column
#Alley, cat feature, could be used for a subset of data, but most don't have this, remove column
#BasementExposure has only one non-missing value, remove column


myplots.plotBoxplot(df,list(missing_data.index)[4:6]+list(missing_data.index)[7:9])
plt.suptitle('SalePrice vs Missing Numerical Fields')
#Fence, may be useful for categorical fitting, remove column
#Fireplace, same,remove column
#GarageQual, 81 homes don't have a garage or info on it. Doesn't correlate consistently, remove
#GarageFinish, could use in categorical fit, remove for now

myplots.plotBoxplot(df,list(missing_data.index)[9:10]+list(missing_data.index)[11:14])
plt.suptitle('SalePrice vs Missing Numerical Fields')
#GarageType, categorical
#GarageCond, doesn't correlate super well, remove
#BsmtFinType2, same, remove
#BsmtQual, could be included in regression, keep

myplots.plotBoxplot(df,list(missing_data.index)[14:16]+list(missing_data.index)[17:18])
plt.suptitle('SalePrice vs Missing Numerical Fields')
#GBsmtFinType1, doesn't correlate super well, remove
#BstmtCond, could be included in regression, keep
#MasVnrType, cat data
myplots.plotScatter(df,['LotFrontage','GarageYrBlt','MasVnrArea'])
plt.suptitle('SalePrice vs Missing Numerical Fields')

 

#%%Build data set for regression (Numerical)

#Conclusion, remove top 7 missing variables.
df_train.drop(columns = list(missing_data.index)[0:8],inplace=True)
#Drop the missing entries from the rest.
df_train = df_train[df_train.BsmtFinType2!=-1].dropna()

#GarageYrBlt (81),Bsmt(37). These are weakly correlated with the SalePrice, but are correlated with other variables as well (GarageYrBuilt with YearBuilt). For now keep the features but drop nan entries. This is effectively dropping houses with no garage or basements. Should go back to this.

#%%Select numerical variables for regression
df_num = df_train.select_dtypes('int64','float64')
y = df_num.SalePrice.values
df_num.drop(columns='SalePrice',inplace=True)
df_num.columns

#%%Try PCA

#normalize features
df_train_norm,_,_ = featureNormalize(df_num)
#%%
from pca import pca
#  Run PCA
U, S, V = pca(df_train_norm.values)
plt.figure()
p=np.cumsum(S)/np.sum(S) #Fraction of total variance
plt.plot(p)
plt.xlabel('Component Inclusion')
plt.ylabel('Fraction of Total Variance')

#Find component where 99% of variance accumulated
ind = np.argwhere(p>.99)[0]
#Total reduction in this case is from 43 to 37 components
#This wouldn't aid in visualization nor faster fitting, so PCA doesn't help much here.
#Still we can find the new representation for our data as follows
Ureduce = U[:,0:ind[0]]
X = np.matmul(Ureduce.T,df_train_norm.values.T).T

#%%Working with old representation
#Select top 2 variables correlated with SalePrice
#ss=sp_corr.index.values[0:2]
#df_num = df_num[ss]

#normalize features
#df_train_norm = featureNormalize(df_num)
#X = df_train_norm.values
#from sklearn.preprocessing import PolynomialFeatures
#polymap = PolynomialFeatures(2,include_bias=False)
#X = polymap.fit_transform(X)

#%%Perform regularized regression 
X = np.column_stack((np.ones(X.shape[0]), X))
# Initialize Theta
initial_theta = np.random.rand(X.shape[1])
initial_theta[0]=50000

Lambda=100.0
labels = ['BasePrice']+ list(df_num.columns)
theta,_,_,_ = trainLinearReg(X, y, Lambda,initial_theta,labels,method = 'BFGS',maxiter=500)
#plot results
plot2DFit(X,y,theta)
#error_train, _ =linearRegCostFunction(X,y,theta,0)
error_train, _ =linearRegLogCostFunction(X,y,theta,0)
print('Error Train = ' + str(error_train))


#PCA


#Employ accuracy metric
#error_val, _ = linearRegCostFunction(Xval,yval,theta,0)
#Try Decision trees, random forest, XGboost
