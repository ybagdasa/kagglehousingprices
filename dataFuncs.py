import pandas as pd
import numpy as np
from pandas.api.types import CategoricalDtype
from sklearn.model_selection import train_test_split,KFold
from featureNormalize import featureNormalize
import matplotlib.pyplot as plt

def loadAndPrepNumData(fname):
    #load data
    #map categorical features with hierarchies to number scale
    df,df_labels = loadAndConvertData(fname,'train_labels.csv')

    #drop columns
    drop_col = ['PoolQC',
     'MiscFeature',
     'Alley',
     'BsmtExposure',
     'Fence',
     'FireplaceQu',
     'LotFrontage']

    #drop specified features and then drop rows with missing data
    df = clearMissingData(df,drop_col,drop_rows=True)
    #make additional features signifying nan values
    #fill in missing values with median values
    missing_data = lookAtMissingData(df)
    print('Remaining missing data from train set:')
    print(missing_data)

    #Select numerical data and compute normalization
    df_num = df.select_dtypes(['int64','float64'])

    return df_num

def loadAndConvertData(fdata,flabels):
    """Function to load data into dataframe and to convert some categorical data values to a number scale"""
    df = pd.read_csv(fdata)
    df_labels = pd.read_csv(flabels) #hand labeled YB
    
    #%Convert cat labels to num for flagged columns###################################
    
    #Convert the string expression in df_labels.Convert2Num to bool
    df_labels.loc[pd.notna(df_labels.Convert2Num),'Convert2Num']=True
    df_labels.loc[pd.isna(df_labels.Convert2Num),'Convert2Num']=False
    
    #define cat lists
    cat_labels = ['Ex','Gd','TA','Fa','Po']
    cat_labels.reverse()
    cat_labels_BsmtFinType = ['Unf','LwQ','Rec','BLQ','ALQ','GLQ']
    cat_labels_Functional = ['Sal','Sev','Maj2','Maj1','Mod','Min2','Min1','Typ']
    cat_labels_Fence = ['MnWw','GdWo','MnPrv','GdPrv']
    
    #define cat type from list
    
    cat_type = CategoricalDtype(categories=cat_labels,ordered=True)
    cat_type_BsmtFinType = CategoricalDtype(categories=cat_labels_BsmtFinType,ordered=True)
    cat_type_Functional = CategoricalDtype(categories=cat_labels_Functional,ordered=True)
    cat_type_Fence = CategoricalDtype(categories=cat_labels_Fence,ordered=True)
    
    #designate the columns marked for conversion as category data types
    
    df['BsmtFinType1']=df['BsmtFinType1'].astype(cat_type_BsmtFinType)
    df['BsmtFinType2']=df['BsmtFinType2'].astype(cat_type_BsmtFinType)
    df['Functional']=df['Functional'].astype(cat_type_Functional)
    df['Fence']=df['Fence'].astype(cat_type_Fence)
    df[df_labels[df_labels.Convert2Num].Id]=df[df_labels[df_labels.Convert2Num].Id].astype(cat_type)
    
    #map the values 
    conv_col = df.select_dtypes(['category']).columns
    df[conv_col] = df[conv_col].apply(lambda x: x.cat.codes)
    
    #specify which data is categorical################################################
    df[df_labels[df_labels.Type=='cat'].Id]=df[df_labels[df_labels.Type=='cat'].Id].astype('category')
    #change data type of converted data to int64 and change its type in df_labels to num
    df[df_labels[df_labels.Convert2Num].Id]=df[df_labels[df_labels.Convert2Num].Id].astype('int64')
    df['BsmtFinType1']=df['BsmtFinType1'].astype('int64')
    df['BsmtFinType2']=df['BsmtFinType2'].astype('int64')
    df['Functional']=df['Functional'].astype('int64')
    df['Fence']=df['Fence'].astype('int64')
    
    df_labels.loc[df_labels.Convert2Num,'Type']='num'
    df_labels.loc[(df_labels.Id=='BsmtFinType1'),'Type']='num'
    df_labels.loc[(df_labels.Id=='BsmtFinType2'),'Type']='num'
    df_labels.loc[(df_labels.Id=='Functional'),'Type']='num'
    df_labels.loc[(df_labels.Id=='Fence'),'Type']='num'
    
    return df,df_labels

def lookAtMissingData(df_train):
    miss_num = (df_train==-1).sum().sort_values(ascending=False)
    miss_cat = df_train.isnull().sum().sort_values(ascending=False)
    missing_data = pd.concat([miss_num,miss_cat],join='outer')
    missing_data=missing_data[missing_data.values!=0]
    missing_data.sort_values(ascending=False,inplace=True)
    return missing_data

def clearMissingData(df_train, drop_col, drop_rows=True):
    df_train.drop(columns = drop_col,inplace=True)

    #drop rows with missing data
    if drop_rows:
        df_train.dropna(inplace = True)
        df_train = df_train[df_train!=-1].dropna()
    return df_train

def get_test_train_sets(df_num,mu,std,cv_frac=.5):
    df_train, df_test = train_test_split(df_num, test_size=cv_frac)

    X,y,Id,labels = getArrayData(df_train,mu,std)
    Xval,yval,Id_val,_ = getArrayData(df_test,mu,std)
    
    
    return X,y,Xval,yval,Id,Id_val,labels

def getArrayData(df,mu,std):
    if 'SalePrice' in df.columns:    
        y = np.log(df.SalePrice.values)
    else:
        y = np.array([])
    Id = df['Id']
    df = df.drop(columns=['SalePrice','Id'],errors='ignore')
    labels = ['Base Price'] + list(df.columns)
    (df_norm,_,_) = featureNormalize(df,mu,std)
    X = df_norm.values
    X = np.column_stack((np.ones(X.shape[0]), X))
    
    return X,y,Id,labels


def computeResiduals(X,y,theta):
    res = X@theta-y
    return res

def identifyHighLeverage(df_dum,thresh = .6):
    X = df_dum.values
    #compute leverage matrix
    H = X@np.linalg.inv(X.T@X)@X.T
    H_diag = np.diag(H)

    #plot leverage ditribution
    plt.figure(figsize= (8,4))
    cnts,xbins,_ = plt.hist(H_diag, bins=50,log=True)
    plt.vlines((X.shape[1] + 1)/X.shape[0],0,max(cnts))
    plt.xlabel('Leverage')
    plt.ylabel('Counts')
    
    #return mask of high leverage points
    ind = H_diag>thresh
    return ind

def identifyOutliers(X,y,Id, Xval,yval,Id_val, theta,sig_cut=6):
    '''Computes outliers from fit residuals and returns Id dataframe.'''
    #compute residuals
    res_train= computeResiduals(X,y,theta)
    res_val= computeResiduals(Xval,yval,theta)

    #identify outliers sig_cut simga away
    ind1 = abs(res_train)>sig_cut*np.std(res_train)#these are shuffled indices
    ind2 = abs(res_val)>sig_cut*np.std(res_val)#these are shuffled indices
    Id[ind1]#go back to Id to find original dataframe entry
    Id_val[ind2]#go back to Id to find original dataframe entry
    outlier_Ids = pd.concat([Id[ind1],Id_val[ind2]]) #dataframe of outlier Ids

    return outlier_Ids