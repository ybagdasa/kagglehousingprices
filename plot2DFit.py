#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

def plot2DFit(X,y,theta):
    """
    Plots y vs X data in a 3D scatter plot along with a surface plot of 2D linear function defined by best fit parameters theta. Returns handle to axes.
    Created on Wed Aug 28 10:25:39 2019
    
    @author: yelena bagdasarova
    """

    fig = plt.figure()
    #Data scatter plot
    ax = Axes3D(fig)
    ax.scatter(X[:,1],X[:,2],y)
    ax.set_xlabel('$X_1$')#$tex$ typsetting
    ax.set_ylabel('$X_2$')
    ax.set_zlabel('$Y$')
    ax.set_title('Fit Results')
    
    #Best fit surface plot
    x_1 = np.linspace(min(X[:,1]),max(X[:,1]))
    x_2 = np.linspace(min(X[:,2]),max(X[:,2]))
    
    X_1, X_2 = np.meshgrid(x_1,x_2)
    xx_1 = np.reshape(X_1,np.size(x_1)*np.size(x_2),-1)
    xx_2 = np.reshape(X_2,np.size(x_1)*np.size(x_2),-1)
    
    yy = theta[0] + theta[1]*xx_1 + theta[2]*xx_2
    YY = np.reshape(yy,np.shape(X_1))
    
    ax.plot_surface(X_1,X_2,YY.T, rstride=8, cstride=8, alpha=0.3,
                    cmap=cm.coolwarm, linewidth=0, antialiased=False)
    
    return ax